package org.example.SystemCityTransportSB.Repositories;

import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
@Transactional(readOnly = true)
public interface ParkingZoneRepo extends JpaRepository<ParkingZone, UUID> {

    ParkingZone findFirstByOrderByIdDesc();

    List<ParkingZone> findAllByParkingBicyclesAllowedTrue();

    List<ParkingZone> findAllByParkingMotorizedScootersAllowedTrue();
}

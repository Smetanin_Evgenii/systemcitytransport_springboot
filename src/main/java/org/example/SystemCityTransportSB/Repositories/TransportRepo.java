package org.example.SystemCityTransportSB.Repositories;

import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.example.SystemCityTransportSB.Model.Entity.TransportConditionEnum;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(readOnly = true)
public interface TransportRepo extends JpaRepository<Transport, String> {


    @Query("SELECT u FROM Transport u WHERE u.id = :id")
    Transport getById(@Param(value = "id") String id);

    @Query("SELECT u FROM Transport u WHERE u.type = :transportType AND u.condition NOT IN :transportConditionExcluded AND u.currentParkingZone = :parkingZone AND u.isFree = :isTransportFree AND u.chargePercent >= :chargePercent")
    List<Transport> findTransport(@Param(value = "transportType") TransportTypeEnum transportType,
                                  @Param(value = "transportConditionExcluded") TransportConditionEnum transportConditionExcluded,
                                  @Param(value = "parkingZone") ParkingZone parkingZone,
                                  @Param(value = "isTransportFree") boolean isTransportFree,
                                  @Param(value = "chargePercent") Integer chargePercent);

    @Query("SELECT u FROM Transport u WHERE u.condition NOT IN :transportConditionExcluded AND u.currentParkingZone = :parkingZone AND u.isFree = :isTransportFree AND u.chargePercent >= :chargePercent")
    List<Transport> findAnyTransport(@Param(value = "transportConditionExcluded") TransportConditionEnum transportConditionExcluded,
                                     @Param(value = "parkingZone") ParkingZone parkingZone,
                                     @Param(value = "isTransportFree") boolean isTransportFree,
                                     @Param(value = "chargePercent") Integer chargePercent);

    @Query("SELECT u FROM Transport u WHERE u.type = :transportType")
    List<Transport> findTransportByType(@Param(value = "transportType") TransportTypeEnum transportType);

    @Query("SELECT u FROM Transport u WHERE u.isFree = :isTransportFree")
    List<Transport> findTransportByStatus(@Param(value = "isTransportFree") boolean isTransportFree);

    @Query("SELECT u FROM Transport u WHERE u.currentParkingZone = :parkingZone")
    List<Transport> findTransportByParkingZone(@Param(value = "parkingZone") ParkingZone parkingZone);

    @Modifying
    @Transactional
    @Query("update Transport u set u.currentParkingZone = :parkingZone  where u.id = :id")
    void changeParkingZone(@Param(value = "id") String id,
                           @Param(value = "parkingZone") ParkingZone parkingZone);

    @Modifying
    @Transactional
    @Query("update Transport u set u.isFree = FALSE where u.id = :id")
    void bookTransport(@Param(value = "id") String id);

    @Modifying
    @Transactional
    @Query("update Transport u set u.isFree = TRUE where u.id = :id")
    void unBookTransport(@Param(value = "id") String id);
}


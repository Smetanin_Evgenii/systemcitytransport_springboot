package org.example.SystemCityTransportSB.Repositories;

import org.example.SystemCityTransportSB.Model.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Repository
@Transactional(readOnly = true)
public interface UserRepo extends JpaRepository<User, UUID> {

    User findByEmail(String email);

    User findFirstByOrderByIdDesc();

    boolean existsByEmail(String email);
}

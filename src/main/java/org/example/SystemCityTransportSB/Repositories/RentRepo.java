package org.example.SystemCityTransportSB.Repositories;

import org.example.SystemCityTransportSB.Model.Dto.UserDto;
import org.example.SystemCityTransportSB.Model.Entity.Rent;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
@Transactional(readOnly = true)
public interface RentRepo extends JpaRepository<Rent, UUID> {

    @Query("SELECT u FROM Rent u")
    Page<Rent> findAllPage(Pageable pageable);

    @Query("SELECT u FROM Rent u WHERE u.renter = :user")
    List<Rent> findByUser(@Param(value = "user") UserDto user);

    @Query("SELECT u FROM Rent u WHERE u.renter = :user")
    Page<Rent> findByUserPage(@Param(value = "user") UserDto user,
                              Pageable pageable);

    @Query("SELECT u FROM Rent u WHERE u.transport = :transport")
    List<Rent> findByTransport(@Param(value = "transport") Transport transport);

    @Query("SELECT u FROM Rent u WHERE u.transport = :transport")
    Page<Rent> findByTransportPage(@Param(value = "transport") Transport transport,
                                   Pageable pageable);

    @Query("SELECT u FROM Rent u WHERE u.isClosed = :isClosed")
    List<Rent> findByStatus(@Param(value = "isClosed") boolean isClosed);

    @Query("SELECT u FROM Rent u WHERE u.isClosed = :isClosed")
    Page<Rent> findByStatusPage(@Param(value = "isClosed") boolean isClosed,
                                Pageable pageable);

    @Query("SELECT u FROM Rent u WHERE u.transport = :transport AND u.isClosed = FALSE")
    Rent findByTransportAndClosedIsFalse(@Param(value = "transport") Transport transport);

    @Query("SELECT u FROM Rent u WHERE u.renter = :user AND u.isClosed = FALSE")
    List<Rent> findByUserAndClosedIsFalse(@Param(value = "user") UserDto user);
}

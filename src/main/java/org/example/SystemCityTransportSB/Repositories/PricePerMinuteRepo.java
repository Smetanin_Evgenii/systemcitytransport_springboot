package org.example.SystemCityTransportSB.Repositories;

import org.example.SystemCityTransportSB.Model.Entity.PricePerMinute;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface PricePerMinuteRepo extends JpaRepository<PricePerMinute, TransportTypeEnum> {

    PricePerMinute findByType(TransportTypeEnum type);
}

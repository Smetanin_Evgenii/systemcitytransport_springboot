package org.example.SystemCityTransportSB.Servlets;

import org.example.SystemCityTransportSB.Model.Dto.UserDto;
import org.example.SystemCityTransportSB.Model.Entity.User;
import org.example.SystemCityTransportSB.Services.Impl.UserAccessServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    UserAccessServiceImpl userAccessService;

    @Autowired
    public LoginServlet(UserAccessServiceImpl userAccessService) {
        this.userAccessService = userAccessService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        req.getRequestDispatcher("/Login.jsp").include(req, resp);
//        try (PrintWriter printWriter = resp.getWriter()) {
//            printWriter.write("Please enter your login and password");
//        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        ServletContext servletContext = getServletContext();
        try {
//            User user = UserAccessServiceImpl.login(login, password);
            User user = userAccessService.login(login, password);
            HttpSession session = req.getSession(true);
            session.setAttribute("user", new UserDto(user));
            resp.setCharacterEncoding("UTF-8");

            servletContext.getRequestDispatcher("/UserSecured/Main.jsp").forward(req, resp);
        } catch (Exception e) {
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            req.setAttribute("error", "Invalid login or password");
            servletContext.getRequestDispatcher("/Login.jsp").forward(req, resp);
        }
    }
}

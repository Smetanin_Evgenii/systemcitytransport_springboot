package org.example.SystemCityTransportSB.Servlets;

import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import static org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum.BICYCLE;
import static org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum.MOTORIZEDSCOOTER;

@WebServlet(name = "MainServlet", urlPatterns = "/UserSecured/main")
public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        req.getRequestDispatcher("/userSecured/Main.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String latitude;
        String longitude;
        String transportId;

        String password = req.getParameter("password");
        ServletContext servletContext = getServletContext();
        //todo
        try {
            latitude = req.getParameter("latitude");
            longitude = req.getParameter("latitude");
            transportId = req.getParameter("transportId");
        } catch (NullPointerException e) {
        }
        try {
            if (req.getParameter("findBicycle") != null || req.getParameter("findScooter") != null) {
                TransportTypeEnum type = (req.getParameter("findBicycle") != null)?
                        BICYCLE : MOTORIZEDSCOOTER;
                //call TransportFinderService

                try (PrintWriter printWriter = resp.getWriter()) {
                    printWriter.write("The closest " + type.toString().toLowerCase() + " is available is at "
                            + "Please enter the following id in the corresponding field and press the \"rentTransport\" button ");
                }
            } else if (req.getParameter("rentTransport") != null) {
                //call RentService, add rent to session attributes, forward to rent jsp

                servletContext.getRequestDispatcher("/userSecured/Rent.jsp").forward(req, resp);
            }
            resp.setCharacterEncoding("UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            servletContext.getRequestDispatcher("/userSecured/Main.jsp").forward(req, resp);
        }
    }
}

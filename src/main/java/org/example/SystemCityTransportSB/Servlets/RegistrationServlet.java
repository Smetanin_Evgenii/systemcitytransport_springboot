package org.example.SystemCityTransportSB.Servlets;

import org.example.SystemCityTransportSB.Services.Impl.UserRegistrationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RegistrationServlet", urlPatterns = "/registration")
public class RegistrationServlet extends HttpServlet {

    UserRegistrationServiceImpl userRegistrationService;

    @Autowired
    public RegistrationServlet(UserRegistrationServiceImpl userRegistrationService){
        this.userRegistrationService = userRegistrationService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        req.getRequestDispatcher("/Registration.jsp").include(req, resp);
//        try (PrintWriter printWriter = resp.getWriter()) {
//            printWriter.write("Please enter your email and password to register.");
//        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String confirmPassword = req.getParameter("confirmPassword");
        ServletContext servletContext = getServletContext();
        try {
            //old code before JPA
//            UserRegistrationServiceImpl.registerNewUser(login, password, confirmPassword);
            userRegistrationService.registerNewUser(login, password, confirmPassword);
            resp.setCharacterEncoding("UTF-8");

            servletContext.getRequestDispatcher("/Login.jsp").forward(req, resp);
        } catch (Exception e) {
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            req.setAttribute("error", "Invalid login or password");
            servletContext.getRequestDispatcher("/Registration.jsp").forward(req, resp);
        }
    }
}

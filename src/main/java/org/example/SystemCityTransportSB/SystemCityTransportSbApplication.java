package org.example.SystemCityTransportSB;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Collections;

@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories
public class SystemCityTransportSbApplication {

	public static void main(String[] args) {

		SpringApplication app = new SpringApplication(SystemCityTransportSbApplication.class);
		app.setDefaultProperties(Collections
				.singletonMap("server.port", "8081"));
		app.run(args);
	}


}
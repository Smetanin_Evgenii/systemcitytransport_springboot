package org.example.SystemCityTransportSB.Security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.text.MessageFormat;

@Getter
@RequiredArgsConstructor
public enum ErrorCodeEnum {
    VALIDATION_ERROR(422, "Validation error"),
    TRANSPORT_OCCUPIED(423, "Transport is occupied"),
    NOT_IN_PARKING_ZONE(403, "Renter is not in parking zone"),
    MAX_NUMBER_OF_RENTS_EXCEEDED(423, "Too many unclosed rents"),
    INTERNAL_SERVER_ERROR(500, "Internal server error"),
    INSUFFICIENT_FUNDS(144, "Insufficient funds to start rent"),
    ACCESS_DENIED(401, "You don't have permission to access this service"),
    USER_NOT_FOUND(404, "User not found, id={0}",
            HttpStatus.NOT_FOUND);

    ErrorCodeEnum(int code, String messageTemplate) {
        this.code = code;
        this.messageTemplate = messageTemplate;
    }

    ErrorCodeEnum(int code, String messageTemplate, HttpStatus httpStatus) {
        this.code = code;
        this.messageTemplate = messageTemplate;
        this.httpStatus = httpStatus;
    }

    private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    private final String messageTemplate;
    private int code;

    public String getMessage(Object... args) {
        return MessageFormat.format(messageTemplate, args);
    }
}

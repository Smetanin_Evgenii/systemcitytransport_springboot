package org.example.SystemCityTransportSB.Security;

import static org.springframework.util.Assert.*;

public final class ErrorDto {

    private final String code;
    private final String message;

    ErrorDto(String code, String message) {
        notNull(code, "Code cannot be null.");
        hasLength(code, "Code cannot be empty.");

        notNull(message, "Message cannot be null.");
        hasLength(message, "Message cannot be empty");

        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
package org.example.SystemCityTransportSB.Security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class CallContext {
    private UUID id;
    private String email;
    private String userRole;
}
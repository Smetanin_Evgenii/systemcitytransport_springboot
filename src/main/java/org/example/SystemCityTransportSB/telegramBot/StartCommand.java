package org.example.SystemCityTransportSB.telegramBot;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.example.SystemCityTransportSB.Repositories.UserRepo;
import org.example.SystemCityTransportSB.Services.Impl.UserRegistrationServiceImpl;
import org.example.SystemCityTransportSB.Services.Impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.apache.commons.lang3.RandomStringUtils;

import java.math.BigDecimal;

@Component
@Slf4j
public class StartCommand extends BotCommand {

    UserRegistrationServiceImpl userRegistrationService;
    UserServiceImpl userService;
    UserRepo userRepo;

    public StartCommand() {
        super("start", "");
    }

    @Autowired
    public StartCommand(UserRegistrationServiceImpl userRegistrationService,
                        UserServiceImpl userService,
                        UserRepo userRepo) {
        super("start", "");
        this.userRegistrationService = userRegistrationService;
        this.userService = userService;
        this.userRepo = userRepo;
    }

    public StartCommand(String commandIdentifier, String description) {
        super(commandIdentifier, description);
    }

    @Transactional
    @SneakyThrows
    @Override
    public void execute(AbsSender absSender,
                        User user,
                        Chat chat,
                        String[] strings) {

        String username = user.getUserName();

        SendMessage msg = new SendMessage();
        msg.enableMarkdown(false);
        msg.setChatId(chat.getId().toString());
        msg.setParseMode("HTML");

        try {

            try {
                org.example.SystemCityTransportSB.Model.Entity.User renter = userRepo.findByEmail(username);
                msg.setText(renter.getEmail() + ", you are already registered.");

            } catch (Exception e) {
                String password = RandomStringUtils.randomAlphanumeric(10);
                userRegistrationService.registerNewUser(username, password, password);
                org.example.SystemCityTransportSB.Model.Entity.User renter = userRepo.findByEmail(username);
                userRepo.saveAndFlush(renter);
                msg.setText("Registration successful! You have been granted a bonus deposit in the amount of " + renter.getBalance().toString() + " rub. \n"
                + "Your password is " + password + ". You can use it to access our website. It is not needed to use our Telegram bot.");
            }

        } catch (Exception e) {
            msg.setText("An error occurred during registration, please try again later or contact our support.");
        }

        absSender.execute(msg);
    }
}

package org.example.SystemCityTransportSB.telegramBot;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.example.SystemCityTransportSB.Repositories.UserRepo;
import org.example.SystemCityTransportSB.Services.Impl.TransportFinderServiceImpl;
import org.example.SystemCityTransportSB.Services.Impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.util.List;

import static org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum.BICYCLE;
import static org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum.MOTORIZEDSCOOTER;

@Component
@Slf4j
public class SearchCommand extends BotCommand {

    TransportFinderServiceImpl transportFinderService;
    UserServiceImpl userService;
    UserRepo userRepo;

    public SearchCommand() {
        super("search", "");
    }

    @Autowired
    public SearchCommand(
            TransportFinderServiceImpl transportFinderService,
            UserServiceImpl userService,
            UserRepo userRepo) {
        super("search", "");
        this.transportFinderService = transportFinderService;
        this.userService = userService;
        this.userRepo = userRepo;
    }

    public SearchCommand(String commandIdentifier, String description) {
        super(commandIdentifier, description);
    }

    @Transactional
    @SneakyThrows
    @Override
    public void execute(AbsSender absSender,
                        User user,
                        Chat chat,
                        String[] strings) {

        StringBuilder message = new StringBuilder();
        SendMessage msg = new SendMessage();
        msg.enableMarkdown(false);
        msg.setChatId(chat.getId().toString());
        msg.setParseMode("HTML");

        try {
            try {
                List<Transport> vehicles;
                try {
                    TransportTypeEnum type = TransportTypeEnum.valueOf(strings[0].toUpperCase());

                    //в реальном проекте нужно брать координаты пользователя

                    vehicles = transportFinderService.getClosestTransportList(56.847068, 35.912256, type);
                } catch (ArrayIndexOutOfBoundsException e) {
                    vehicles = transportFinderService.getClosestTransportList(56.847068, 35.912256);
                }

                for (Transport t : vehicles
                ) {
                    message.append(t.getId()).append("\n");
                }

            } catch (IllegalArgumentException e) {

                message.append("Please specify desired transport type correctly. Currently available transport types are '")
                        .append(BICYCLE.toString().toLowerCase())
                        .append("' and '")
                        .append(MOTORIZEDSCOOTER.toString().toLowerCase())
                        .append("'.");
            }
            msg.setText(message.toString());

        } catch (Exception e) {
            msg.setText("An error occurred during search, please try again later or contact our support.");
        }
        absSender.execute(msg);
    }
}

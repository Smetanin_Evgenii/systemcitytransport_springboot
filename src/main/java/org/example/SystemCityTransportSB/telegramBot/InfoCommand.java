package org.example.SystemCityTransportSB.telegramBot;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.example.SystemCityTransportSB.Model.Dto.UserDto;
import org.example.SystemCityTransportSB.Model.Entity.Rent;
import org.example.SystemCityTransportSB.Repositories.RentRepo;
import org.example.SystemCityTransportSB.Repositories.UserRepo;
import org.example.SystemCityTransportSB.Services.Impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.util.List;

@Component
@Slf4j
public class InfoCommand extends BotCommand {

    UserServiceImpl userService;
    UserRepo userRepo;
    RentRepo rentRepo;

    public InfoCommand() {
        super("info", "");
    }

    @Autowired
    public InfoCommand(
            UserServiceImpl userService,
            UserRepo userRepo,
            RentRepo rentRepo) {
        super("info", "");
        this.userService = userService;
        this.userRepo = userRepo;
        this.rentRepo = rentRepo;
    }

    public InfoCommand(String commandIdentifier, String description) {
        super(commandIdentifier, description);
    }

    @Transactional
    @SneakyThrows
    @Override
    public void execute(AbsSender absSender,
                        User user,
                        Chat chat,
                        String[] strings) {
        String username = user.getUserName();
        UserDto userDto;
        StringBuilder response = new StringBuilder();
        List<Rent> rents;

        SendMessage msg = new SendMessage();
        msg.enableMarkdown(false);
        msg.setChatId(chat.getId().toString());
        msg.setParseMode("HTML");
        try {
            userDto = new UserDto(userRepo.findByEmail(username));
            rents = rentRepo.findByUserAndClosedIsFalse(userDto);
            response.append("Your balance is: ")
                    .append(userDto.getBalance())
                    .append(" rub.\n")
                    .append("Your number of rides is ")
                    .append(userDto.getNumberOfRides())
                    .append(".\n");

            if (!rents.isEmpty()) {
                response.append("Your current unclosed rents are:");
                for (Rent r : rents) {
                    response.append("\n")
                            .append(r.getStartTime(), 0, r.getStartTime().indexOf("."))
                            .append(" ")
                            .append(r.getTransport().getId());
                }
                response.append("\n");
            } else {
                response.append("You currently have no unclosed rents.\n");
            }
            msg.setText(response.toString());

        } catch (Exception e) {
            msg.setText("An error occurred during command processing, please try again later or contact our support.");
        }
        absSender.execute(msg);
    }
}
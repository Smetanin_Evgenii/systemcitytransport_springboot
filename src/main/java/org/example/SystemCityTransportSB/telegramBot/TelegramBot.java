package org.example.SystemCityTransportSB.telegramBot;

import org.example.SystemCityTransportSB.Repositories.RentRepo;
import org.example.SystemCityTransportSB.Repositories.TransportRepo;
import org.example.SystemCityTransportSB.Repositories.UserRepo;
import org.example.SystemCityTransportSB.Services.Impl.RentServiceImpl;
import org.example.SystemCityTransportSB.Services.Impl.TransportFinderServiceImpl;
import org.example.SystemCityTransportSB.Services.Impl.UserRegistrationServiceImpl;
import org.example.SystemCityTransportSB.Services.Impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
public class TelegramBot extends TelegramLongPollingCommandBot {
    String token = "5030846580:AAG6fHsv0JYa-eIfCNic7rae7x52DB4hqM8";
    String username = "@TverRentBot";

    public TelegramBot() {
        this.register(new StartCommand());
    }

    @Autowired
    public TelegramBot(UserRegistrationServiceImpl userRegistrationService,
                       TransportFinderServiceImpl transportFinderService,
                       UserServiceImpl userService,
                       RentServiceImpl rentService,
                       TransportRepo transportRepo,
                       UserRepo userRepo,
                       RentRepo rentRepo) {

        this.register(new StartCommand(
                userRegistrationService,
                userService,
                userRepo));

        this.register(new InfoCommand(
                userService,
                userRepo,
                rentRepo));

        this.register(new SearchCommand(
                transportFinderService,
                userService,
                userRepo));

        this.register(new TripCommand(
                rentService,
                userRepo,
                transportRepo));

        this.register(new FinishCommand(
                rentService,
                userRepo,
                transportRepo,
                rentRepo));
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    @Override
    public void processNonCommandUpdate(Update update) {
        try {
            SendMessage msg = new SendMessage();
            msg.setChatId(String.valueOf(update.getMessage().getChatId()));
            msg.setText("The message you sent is not a valid command. Please check the list of commands.");
            execute(msg);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}

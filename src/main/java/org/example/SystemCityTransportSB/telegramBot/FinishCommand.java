package org.example.SystemCityTransportSB.telegramBot;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.example.SystemCityTransportSB.Model.Entity.Rent;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.example.SystemCityTransportSB.Repositories.RentRepo;
import org.example.SystemCityTransportSB.Repositories.TransportRepo;
import org.example.SystemCityTransportSB.Repositories.UserRepo;
import org.example.SystemCityTransportSB.Security.ErrorCodeEnum;
import org.example.SystemCityTransportSB.Services.BusinessRuntimeException;
import org.example.SystemCityTransportSB.Services.Impl.RentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;


@Component
@Slf4j
public class FinishCommand extends BotCommand {

    RentServiceImpl rentService;
    UserRepo userRepo;
    TransportRepo transportRepo;
    RentRepo rentRepo;

    public FinishCommand() {
        super("finish", "");
    }

    @Autowired
    public FinishCommand(RentServiceImpl rentService,
                         UserRepo userRepo,
                         TransportRepo transportRepo,
                         RentRepo rentRepo) {
        super("finish", "");
        this.rentService = rentService;
        this.userRepo = userRepo;
        this.transportRepo = transportRepo;
        this.rentRepo = rentRepo;
    }

    @Transactional
    @SneakyThrows
    @Override
    public void execute(AbsSender absSender,
                        User user,
                        Chat chat,
                        String[] strings) {

        String username = user.getUserName();
        String transportId;
        Transport transport;
        Rent rent;

        SendMessage msg = new SendMessage();
        msg.enableMarkdown(false);
        msg.setChatId(chat.getId().toString());
        msg.setParseMode("HTML");
        org.example.SystemCityTransportSB.Model.Entity.User renter;

        a:
        try {
            renter = userRepo.findByEmail(username);

            try {
                if (renter.getEmail().isEmpty() || renter.getEmail().isBlank()) {
                    throw new BusinessRuntimeException(ErrorCodeEnum.USER_NOT_FOUND);
                }
            } catch (BusinessRuntimeException e) {
                msg.setText("It seems that you are not registered in the system. try typing /start command first or contact our support.");
                break a;
            }

            try {
                transportId = strings[0];
            } catch (ArrayIndexOutOfBoundsException e) {
                msg.setText("Please specify the ID of the vehicle which you want to park");
                break a;
            }

            try {
                transport = transportRepo.getById(transportId);
                if (transport.getType() == null) {
                    throw new BusinessRuntimeException(ErrorCodeEnum.VALIDATION_ERROR);
                }
            } catch (BusinessRuntimeException e) {
                msg.setText("It seems you've made a typo. Please check transport ID and try again.");
                break a;
            }

            try {
                rent = rentRepo.findByTransportAndClosedIsFalse(transport);
                if (!rent.getRenter().getId().equals(renter.getId())) {
                    throw new BusinessRuntimeException(ErrorCodeEnum.ACCESS_DENIED);
                }
            } catch (BusinessRuntimeException e) {
                msg.setText("It seems you're trying to finish someone else's rent. Please check transport ID and try again.");
                break a;
            }

            //в реальном проекте нужно брать координаты пользователя
            try {
                rentService.endRent(rent, 56.847068, 35.912256);
                msg.setText("Rent successfully ended! Your bill is " + rent.getBill() + " rub.");
            } catch (BusinessRuntimeException e) {
                msg.setText("It seems you're not in a parking zone.");
                break a;
            }
        } catch (Exception e) {
            msg.setText("An error occurred while finishing rent, please try again later or contact our support.");
        }

        absSender.execute(msg);
    }
}
package org.example.SystemCityTransportSB.telegramBot;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.example.SystemCityTransportSB.Model.Dto.UserDto;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.example.SystemCityTransportSB.Repositories.TransportRepo;
import org.example.SystemCityTransportSB.Repositories.UserRepo;
import org.example.SystemCityTransportSB.Security.ErrorCodeEnum;
import org.example.SystemCityTransportSB.Services.BusinessRuntimeException;
import org.example.SystemCityTransportSB.Services.Impl.RentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Component
@Slf4j
public class TripCommand extends BotCommand {

    RentServiceImpl rentService;
    UserRepo userRepo;
    TransportRepo transportRepo;

    public TripCommand() {
        super("trip", "");
    }

    @Autowired
    public TripCommand(RentServiceImpl rentService,
                       UserRepo userRepo,
                       TransportRepo transportRepo) {
        super("trip", "");
        this.rentService = rentService;
        this.userRepo = userRepo;
        this.transportRepo = transportRepo;
    }

    @Transactional
    @SneakyThrows
    @Override
    public void execute(AbsSender absSender,
                        User user,
                        Chat chat,
                        String[] strings) {

        String username = user.getUserName();
        String transportId;
        Transport transport;

        SendMessage msg = new SendMessage();
        msg.enableMarkdown(false);
        msg.setChatId(chat.getId().toString());
        msg.setParseMode("HTML");
        org.example.SystemCityTransportSB.Model.Entity.User renter;
        UserDto userDto;

        a:
        try {
            renter = userRepo.findByEmail(username);

            try {
                if (renter.getEmail().isEmpty() || renter.getEmail().isBlank()) {
                    throw new BusinessRuntimeException(ErrorCodeEnum.USER_NOT_FOUND);
                }
            } catch (BusinessRuntimeException e) {
                msg.setText("It seems that you are not registered in the system. try typing /start command first or contact our support.");
                break a;
            }

            try {
                transportId = strings[0];
            } catch (ArrayIndexOutOfBoundsException e) {
                msg.setText("Please specify the ID of the vehicle you want to rent");
                break a;
            }

            try {
                transport = transportRepo.getById(transportId);
                if (transport == null) {
                    throw new BusinessRuntimeException(ErrorCodeEnum.VALIDATION_ERROR);
                }
            } catch (BusinessRuntimeException e) {
                msg.setText("It seems you've made a typo. Please check transport ID and try again.");
                break a;
            }

            userDto = new UserDto(renter);
            rentService.startRent(userDto, transport);
            msg.setText("Rent successfully started!");

        } catch (Exception e) {
            msg.setText("An error occurred during rent: " + e.getMessage() + ". \nPlease try again later or contact our support.");
        }

        absSender.execute(msg);
    }
}
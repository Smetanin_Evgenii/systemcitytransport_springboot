package org.example.SystemCityTransportSB.Model.mapper;

import org.example.SystemCityTransportSB.Model.Dto.ParkingZoneDto;
import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ParkingMapper {
    ParkingZoneDto modelToDto(ParkingZone model);
}

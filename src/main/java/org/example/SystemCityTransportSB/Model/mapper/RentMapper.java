package org.example.SystemCityTransportSB.Model.mapper;

import org.example.SystemCityTransportSB.Model.Dto.RentDto;
import org.example.SystemCityTransportSB.Model.Entity.Rent;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RentMapper {
    RentDto modelToDto(Rent model);
}

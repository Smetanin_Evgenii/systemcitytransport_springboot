package org.example.SystemCityTransportSB.Model.mapper;

import org.example.SystemCityTransportSB.Model.Dto.UserDto;
import org.example.SystemCityTransportSB.Model.Entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto modelToDto(User model);
}

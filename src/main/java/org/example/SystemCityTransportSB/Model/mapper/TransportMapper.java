package org.example.SystemCityTransportSB.Model.mapper;

import org.example.SystemCityTransportSB.Model.Dto.TransportDto;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TransportMapper {
    TransportDto modelToDto(Transport model);
}

package org.example.SystemCityTransportSB.Model.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.example.SystemCityTransportSB.Model.Entity.Transport;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RentDto {

    @NotNull
    UUID id;

    @Valid
    UserDto renter;

    @NotBlank
    String startTime;

    String endTime;

    Date start;

    Date end;

    long rentMinutes;

    @Valid
    Transport transport;

    @Valid
    ParkingZone startParkingZone;

    ParkingZone endParkingZone;

    BigDecimal bill = BigDecimal.ZERO;

    boolean isClosed;

    final String SDF_PATTERN = "yyyy-MM-dd HH:mm:ss.SS";
}

package org.example.SystemCityTransportSB.Model.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ParkingZoneDto {

    @NotNull
    UUID id;

    public boolean parkingBicyclesAllowed;

    public boolean parkingMotorizedScootersAllowed;

    @Positive
    long radiusInMetres;

    double latitude;

    double longitude;
}
package org.example.SystemCityTransportSB.Model.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.SystemCityTransportSB.Model.Entity.User;
import org.example.SystemCityTransportSB.Model.Entity.UserRoleEnum;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "UserDto")
@Table(name = "user")
public class UserDto {

    @NotNull
    @Id
    @Column(name = "user_id")
    private UUID id;

    @NotBlank
    @Size(min = 6, max = 19, message
            = "Email must be between 6 and 19 characters")
    @Column(name = "user_email")
    private String email;

    @Column(name = "user_balance")
    private BigDecimal balance;

    @PositiveOrZero
    @Column(name = "user_number_of_rides")
    private int numberOfRides;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "user_role")
    private UserRoleEnum role;

    @Transient
    private double currentLatitude;

    @Transient
    private double currentLongitude;

    public UserDto(User user) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.balance = user.getBalance();
        this.numberOfRides = user.getNumberOfRides();
        this.role = user.getRole();
    }

    public void setCoordinates(double latitude, double longitude) {
        currentLatitude = latitude;
        currentLongitude = longitude;
    }

    public double[] getCoordinates() {
        double[] coordinates = new double[2];
        coordinates[0] = currentLatitude;
        coordinates[1] = currentLongitude;
        return coordinates;
    }
}

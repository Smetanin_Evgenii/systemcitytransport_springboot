package org.example.SystemCityTransportSB.Model.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.example.SystemCityTransportSB.Model.Entity.TransportConditionEnum;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransportDto {
    @NotEmpty
    @NotBlank
    private String id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TransportTypeEnum type;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TransportConditionEnum condition;

    @Valid
    private ParkingZone currentParkingZone;

    private boolean isFree;

    private Integer chargePercent;

    private Integer maxSpeedKMPH;
}

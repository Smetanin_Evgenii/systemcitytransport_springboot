package org.example.SystemCityTransportSB.Model.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.SystemCityTransportSB.Model.Dto.UserDto;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum.BICYCLE;
import static org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum.MOTORIZEDSCOOTER;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "Rent")
@Table(name = "rent")
public class Rent {

    @NotNull
    @Id
    @Column(name = "rent_id")
    UUID id;

    @Valid
    @OneToOne
    @JoinColumn(name = "user_id")
    UserDto renter;

    @NotBlank
    @Column(name = "start_time")
    String startTime;

    @Column(name = "end_time")
    String endTime;

    @Transient
    Date start;

    @Transient
    Date end;

    @Transient
    long rentMinutes;

    @Valid
    @OneToOne
    @JoinColumn(name = "transport_id")
    Transport transport;

    @Valid
    @OneToOne
    @JoinColumn(name = "start_parking_zone_id")
    ParkingZone startParkingZone;

    @OneToOne
    @JoinColumn(name = "end_parking_zone_id")
    ParkingZone endParkingZone;

    @Column(name = "bill")
    BigDecimal bill = BigDecimal.ZERO;

    @Column(name = "is_closed")
    boolean isClosed;

    @Transient
    final String SDF_PATTERN = "yyyy-MM-dd HH:mm:ss.SS";


    public Rent(UUID id, UserDto renter, Transport transport, ParkingZone startParkingZone) {
        this.id = id;
        this.renter = renter;
        this.transport = transport;
        this.startParkingZone = startParkingZone;

        SimpleDateFormat sdf = new SimpleDateFormat(SDF_PATTERN);
        Date now = new Date();
        startTime = sdf.format(now);
        start = now;
        isClosed = false;
    }

    public void endRent(ParkingZone endParkingZone, PricePerMinute pricePerMinute) {
        SimpleDateFormat sdf;
        Date now;

        if (isParkingZoneSuitable(transport, endParkingZone)) {

            this.endParkingZone = endParkingZone;
            sdf = new SimpleDateFormat(SDF_PATTERN);
            now = new Date();
            endTime = sdf.format(now);
            end = now;

            if (start == null) {
                try {
                    start = sdf.parse(startTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            long msDiff = end.getTime() - start.getTime();
            rentMinutes = (msDiff) / 60000;
            if (rentMinutes == 0) {
                rentMinutes = 1;
            }
            rentMinutes += 10;
            calculateBill(pricePerMinute);
            isClosed = true;

        } else {
            System.out.println("Parking a " + transport.getType().toString().toLowerCase() + " not allowed here");
        }
    }

    public boolean isParkingZoneSuitable(Transport transport, ParkingZone endParkingZone) {
        boolean result;

        result = transport.getType().equals(BICYCLE) && endParkingZone.parkingBicyclesAllowed
                || transport.getType().equals(MOTORIZEDSCOOTER) && endParkingZone.parkingMotorizedScootersAllowed;
        return result;
    }

    public void calculateBill(PricePerMinute pricePerMinute) {
        bill = pricePerMinute.getPricePerMinute().multiply(BigDecimal.valueOf(rentMinutes));
    }
}

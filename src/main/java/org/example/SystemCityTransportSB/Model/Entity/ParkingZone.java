package org.example.SystemCityTransportSB.Model.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "ParkingZone")
@Table(name = "parking_zone")
public class ParkingZone {

    @NotNull
    @Id
    @Column(name = "parking_zone_id")
    UUID id;

    @Column(name = "parking_bicycles_allowed")
    public boolean parkingBicyclesAllowed;

    @Column(name = "parking_motorized_scooters_allowed")
    public boolean parkingMotorizedScootersAllowed;

    @Positive
    @Column(name = "radius_m")
    long radiusInMetres;

    @Column(name = "latitude")
    double latitude;

    @Column(name = "longitude")
    double longitude;

    public boolean isRenterInParkingZone(double latitude, double longitude) {
        boolean output;
        output = calculateDistanceToParkingZoneCenter(latitude, longitude) <= (double) radiusInMetres;
        return output;
    }

    private double calculateDistanceToParkingZoneCenter(double latitude, double longitude) {
        double distanceInMetres;
        distanceInMetres = Math.sqrt(Math.pow((Math.abs(this.latitude - latitude) * 111000), 2)
                + Math.pow(Math.abs(111000 * Math.cos(this.latitude * (Math.PI / 180)) * (this.longitude - longitude)), 2));
        return distanceInMetres;
    }
}


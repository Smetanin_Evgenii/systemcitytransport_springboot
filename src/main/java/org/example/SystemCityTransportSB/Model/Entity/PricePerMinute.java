package org.example.SystemCityTransportSB.Model.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "PricePerMinute")
@Table(name = "price_per_minute")
public class PricePerMinute {

    @NotNull
    @Id
    @Enumerated(EnumType.STRING)
    @Column(name = "transport_type")
    TransportTypeEnum type;

    @Positive
    @Column(name = "price")
    BigDecimal pricePerMinute;
}

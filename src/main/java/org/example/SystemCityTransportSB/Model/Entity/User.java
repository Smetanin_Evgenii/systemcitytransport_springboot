package org.example.SystemCityTransportSB.Model.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "User")
@Table(name = "user")
public class User {

    @NotNull
    @Id
    @Column(name = "user_id")
    private UUID id;

    @NotBlank
    @Size(min = 6, max = 19, message
            = "Email must be between 6 and 19 characters")
    @Column(name = "user_email")
    private String email;

    @NotBlank
    @Size(min = 5, max = 19, message
            = "Password must be between 5 and 19 characters")
    @Column(name = "user_password")
    private String password;

    @Column(name = "user_balance")
    private BigDecimal balance;

    @PositiveOrZero
    @Column(name = "user_number_of_rides")
    private int numberOfRides;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "user_role")
    private UserRoleEnum role;

    public User(UUID id, String email, String password, BigDecimal balance, int numberOfRides, UserRoleEnum role) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.balance = balance;
        this.numberOfRides = numberOfRides;
        this.role = role;
    }
}
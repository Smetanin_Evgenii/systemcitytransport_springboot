package org.example.SystemCityTransportSB.Model.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Transport")
@Table(name = "transport")
public class Transport {

    @NotEmpty
    @NotBlank
    @Id
    @Column(name = "transport_id")
    private String id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "transport_type")
    private TransportTypeEnum type;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "transport_condition")
    private TransportConditionEnum condition;

    @Valid
    @OneToOne
    @JoinColumn(name = "transport_parking_zone_id")
    private ParkingZone currentParkingZone;

    @Column(name = "is_transport_free")
    private boolean isFree;

    @Column(name = "motorized_scooter_charge_percent")
    private Integer chargePercent;

    @Column(name = "motorized_scooter_max_speed_kmph")
    private Integer maxSpeedKMPH;

    public UUID getCurrentParkingZoneId() {
        return currentParkingZone.getId();
    }
}

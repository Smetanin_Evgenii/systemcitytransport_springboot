package org.example.SystemCityTransportSB.controller;

import org.example.SystemCityTransportSB.Model.Dto.ParkingZoneDto;
import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.example.SystemCityTransportSB.Model.mapper.ParkingMapper;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.example.SystemCityTransportSB.Repositories.ParkingZoneRepo;
import org.example.SystemCityTransportSB.Services.Impl.ParkingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/parking")
public class ParkingZoneController {

    private final AccessCheckerImpl accessChecker;
    private final ParkingServiceImpl parkingService;
    private final ParkingZoneRepo parkingZoneRepo;
    private final ParkingMapper parkingMapper;

    @Autowired
    public ParkingZoneController(AccessCheckerImpl accessChecker,
                                 ParkingZoneRepo parkingZoneRepo,
                                 ParkingServiceImpl parkingService,
                                 ParkingMapper parkingMapper) {
        this.accessChecker = accessChecker;
        this.parkingZoneRepo = parkingZoneRepo;
        this.parkingService = parkingService;
        this.parkingMapper = parkingMapper;
    }


    @GetMapping(value = "/closest", produces = MediaType.APPLICATION_JSON_VALUE)
    public ParkingZoneDto getClosestParkingZoneId(@RequestParam("latitude") double latitude,
                                                  @RequestParam("longitude") double longitude,
                                                  @RequestParam("type") TransportTypeEnum type) {
        ParkingZone parkingZone = parkingService.getClosestParkingZone(latitude, longitude, type);
        return parkingMapper.modelToDto(parkingZone);
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ParkingZoneDto> getAllParkingZones() {
        List<ParkingZone> parkingZones = parkingZoneRepo.findAll();
        List<ParkingZoneDto> parkingZoneDtos = new ArrayList<>();

        for (ParkingZone p : parkingZones) {
            parkingZoneDtos.add(parkingMapper.modelToDto(p));
        }
        return parkingZoneDtos;
    }

    @DeleteMapping("/remove")
    public void deleteParkingZone(@RequestBody @Validated ParkingZone parkingZone,
                                  HttpServletRequest request) {
        accessChecker.checkAccess(request);
        parkingService.deleteParkingZone(parkingZone);
    }

    @DeleteMapping("/remove_by_id")
    public void deleteParkingZoneById(@RequestParam("id") String id,
                                      HttpServletRequest request) {
        accessChecker.checkAccess(request);
        parkingService.deleteParkingZone(id);
    }

    @PostMapping("/new")
    public void addParkingZone(@RequestBody @Validated ParkingZone parkingZone,
                               HttpServletRequest request) {
        accessChecker.checkAccess(request);
        parkingService.addNewParkingZoneToDB(parkingZone);
    }

    @PutMapping("/update")
    public void addOrUpdateParkingZone(@RequestBody @Validated ParkingZone parkingZone,
                                       HttpServletRequest request) {
        accessChecker.checkAccess(request);
        parkingService.updateParkingZoneInfo(parkingZone);
    }

    @DeleteMapping("/delete")
    public void deleteParkingZone(@RequestParam("id") @NotBlank String id,
                                  HttpServletRequest request) {
        accessChecker.checkAccess(request);
        parkingService.deleteParkingZone(id);
    }
}

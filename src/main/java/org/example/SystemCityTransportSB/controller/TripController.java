package org.example.SystemCityTransportSB.controller;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.example.SystemCityTransportSB.Model.Dto.RentDto;
import org.example.SystemCityTransportSB.Model.Dto.UserDto;
import org.example.SystemCityTransportSB.Model.Entity.Rent;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.example.SystemCityTransportSB.Model.Entity.User;
import org.example.SystemCityTransportSB.Model.Entity.UserRoleEnum;
import org.example.SystemCityTransportSB.Model.mapper.RentMapper;
import org.example.SystemCityTransportSB.Repositories.RentRepo;
import org.example.SystemCityTransportSB.Repositories.UserRepo;
import org.example.SystemCityTransportSB.Security.ErrorCodeEnum;
import org.example.SystemCityTransportSB.Services.BusinessRuntimeException;
import org.example.SystemCityTransportSB.Services.Impl.RentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;


@RestController
@RequestMapping("/trips")
public class TripController {
    RentServiceImpl rentService;
    private final AccessCheckerImpl accessChecker;
    private final UserRepo userRepo;
    private final RentRepo rentRepo;
    private final RentMapper rentMapper;

    @Autowired
    public TripController(AccessCheckerImpl accessChecker,
                          UserRepo userRepo,
                          RentServiceImpl rentService,
                          RentRepo rentRepo,
                          RentMapper rentMapper) {
        this.accessChecker = accessChecker;
        this.userRepo = userRepo;
        this.rentService = rentService;
        this.rentRepo = rentRepo;
        this.rentMapper = rentMapper;
    }

    @GetMapping(value = "/my", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RentDto> findMyRents(HttpServletRequest request) {
        String token = request.getHeader(AUTHORIZATION);
        String jwtSecret = "secret";
        Claims claims = Jwts.parser().setSigningKey(jwtSecret)
                .parseClaimsJws(token).getBody();

        UserDto user = new UserDto(userRepo.findById(UUID.fromString(claims.getSubject()))
                .orElseThrow(() -> new BusinessRuntimeException(ErrorCodeEnum.USER_NOT_FOUND, claims.getSubject())));
        List<Rent> rents = rentRepo.findByUser(user);
        List<RentDto> rentDtos = new ArrayList<>();

        for (Rent r : rents) {
            rentDtos.add(rentMapper.modelToDto(r));
        }
        return rentDtos;
    }


    @PostMapping(value = "/find_by_user", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RentDto> findByUser(@RequestBody @Validated UserDto user,
                                    HttpServletRequest request) {
        accessChecker.checkAccess(request);
        List<Rent> rents;
        rents = rentRepo.findByUser(user);
        List<RentDto> rentDtos = new ArrayList<>();

        for (Rent r : rents) {
            rentDtos.add(rentMapper.modelToDto(r));
        }
        return rentDtos;
    }

    @PostMapping(value = "/find_by_user_page", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Rent> findByUserPage(@RequestBody @Validated UserDto user,
                                        HttpServletRequest request,
                                        @PageableDefault(size = 5) Pageable pageable) {
        accessChecker.checkAccess(request);
        Page<Rent> rents;
        rents = rentRepo.findByUserPage(user, pageable);
        return rents;
    }

    @PostMapping(value = "/find_by_transport", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RentDto> findByTransport(@RequestBody @Validated Transport transport,
                                         HttpServletRequest request) {
        accessChecker.checkAccess(request);
        List<Rent> rents = rentRepo.findByTransport(transport);
        List<RentDto> rentDtos = new ArrayList<>();

        for (Rent r : rents) {
            rentDtos.add(rentMapper.modelToDto(r));
        }
        return rentDtos;
    }

    @PostMapping(value = "/find_by_transport_page", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Rent> findByTransportPage(@RequestBody @Validated Transport transport,
                                             HttpServletRequest request,
                                             @PageableDefault(size = 5) Pageable pageable) {
        accessChecker.checkAccess(request);
        Page<Rent> rents = rentRepo.findByTransportPage(transport, pageable);
        return rents;
    }

    @GetMapping(value = "/find_by_status", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RentDto> findByStatus(@RequestParam("is_closed") boolean isClosed,
                                      HttpServletRequest request) {
        accessChecker.checkAccess(request);
        List<Rent> rents;
        rents = rentRepo.findByStatus(isClosed);
        List<RentDto> rentDtos = new ArrayList<>();

        for (Rent r : rents) {
            rentDtos.add(rentMapper.modelToDto(r));
        }
        return rentDtos;
    }

    @GetMapping(value = "/find_by_status_page", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Rent> findByStatusPage(@RequestParam("is_closed") boolean isClosed,
                                          HttpServletRequest request,
                                          @PageableDefault(size = 5) Pageable pageable) {
        accessChecker.checkAccess(request);
        Page<Rent> rents;
        rents = rentRepo.findByStatusPage(isClosed, pageable);
        return rents;
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RentDto> getAllTrips(HttpServletRequest request) {
        List<Rent> rents;
        List<RentDto> rentDtos = new ArrayList<>();

        String token = request.getHeader(AUTHORIZATION);
        String jwtSecret = "secret";
        Claims claims = Jwts.parser().setSigningKey(jwtSecret)
                .parseClaimsJws(token).getBody();

        UserDto user = new UserDto(userRepo.findById(UUID.fromString(claims.getSubject()))
                .orElseThrow(() -> new BusinessRuntimeException(ErrorCodeEnum.USER_NOT_FOUND, claims.getSubject())));
        if (user.getRole().equals(UserRoleEnum.ADMIN)) {
            rents = rentRepo.findAll();
        } else {
            rents = rentRepo.findByUser(user);
        }

        for (Rent r : rents) {
            rentDtos.add(rentMapper.modelToDto(r));
        }
        return rentDtos;
    }

    @GetMapping(value = "/all_page", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Rent> getAllTripsPage(HttpServletRequest request,
                                         @PageableDefault(size = 5) Pageable pageable) {
        Page<Rent> rents;

        String token = request.getHeader(AUTHORIZATION);
        String jwtSecret = "secret";
        Claims claims = Jwts.parser().setSigningKey(jwtSecret)
                .parseClaimsJws(token).getBody();

        UserDto user = new UserDto(userRepo.findById(UUID.fromString(claims.getSubject()))
                .orElseThrow(() -> new BusinessRuntimeException(ErrorCodeEnum.USER_NOT_FOUND, claims.getSubject())));
        if (user.getRole().equals(UserRoleEnum.ADMIN)) {
            rents = rentRepo.findAllPage(pageable);
        } else {
            rents = rentRepo.findByUserPage(user, pageable);
        }
        return rents;
    }


    @PostMapping(value = "/start", produces = MediaType.APPLICATION_JSON_VALUE)
    public Rent startTrip(@RequestBody @Validated Transport transport,
                          HttpServletRequest request) {

        String token = request.getHeader(AUTHORIZATION);
        String jwtSecret = "secret";
        Claims claims = Jwts.parser().setSigningKey(jwtSecret)
                .parseClaimsJws(token).getBody();

        User user = userRepo.findById(UUID.fromString(claims.getSubject()))
                .orElseThrow(() -> new BusinessRuntimeException(ErrorCodeEnum.USER_NOT_FOUND, claims.getSubject()));
        return rentService.startRent(new UserDto(user), transport);
    }

    @PostMapping("/end")
    public String endTrip(@RequestBody @Validated Rent rent,
                          @RequestParam("latitude") double latitude,
                          @RequestParam("longitude") double longitude) {
        rentService.endRent(rent, latitude, longitude);
        return "Trip ended successfully";
    }

    @DeleteMapping("/remove")
    public void deleteTrip(@RequestBody @Validated Rent rent,
                           HttpServletRequest request) {
        accessChecker.checkAccess(request);
        rentRepo.delete(rent);
    }

    @PutMapping("/update")
    public void addOrUpdateTrip(@RequestBody @Validated Rent rent,
                                HttpServletRequest request) {
        accessChecker.checkAccess(request);
        rentRepo.saveAndFlush(rent);
    }
}

package org.example.SystemCityTransportSB.controller;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.NoArgsConstructor;
import org.example.SystemCityTransportSB.Model.Entity.UserRoleEnum;
import org.example.SystemCityTransportSB.Security.ErrorCodeEnum;
import org.example.SystemCityTransportSB.Services.BusinessRuntimeException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@NoArgsConstructor
@Service
@Transactional(readOnly = true)
public class AccessCheckerImpl implements AccessChecker {
    //change to true to enable access checking or vice versa
    boolean checkAccess = true;

    @Override
    public void checkAccess(HttpServletRequest request) {
        String token = request.getHeader(AUTHORIZATION);
        String jwtSecret = "secret";
        Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();

        if (checkAccess) {
            if (!UserRoleEnum.valueOf(claims.get("role", String.class)).equals(UserRoleEnum.ADMIN)) {
                throw new BusinessRuntimeException(ErrorCodeEnum.ACCESS_DENIED);
            }
        }
    }
}

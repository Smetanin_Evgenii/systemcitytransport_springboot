package org.example.SystemCityTransportSB.controller;

import org.example.SystemCityTransportSB.Model.Dto.TransportDto;
import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.example.SystemCityTransportSB.Model.mapper.TransportMapper;
import org.example.SystemCityTransportSB.Repositories.TransportRepo;
import org.example.SystemCityTransportSB.Services.Impl.TransportFinderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/vehicles")
public class VehicleController {

    private final AccessCheckerImpl accessChecker;
    private final TransportRepo transportRepo;
    private final TransportFinderServiceImpl transportFinderService;
    private final TransportMapper transportMapper;

    @Autowired
    public VehicleController(AccessCheckerImpl accessChecker,
                             TransportRepo transportRepo,
                             TransportFinderServiceImpl transportFinderService,
                             TransportMapper transportMapper) {
        this.accessChecker = accessChecker;
        this.transportRepo = transportRepo;
        this.transportFinderService = transportFinderService;
        this.transportMapper = transportMapper;
    }

    @PutMapping("/update")
    public void addOrUpdateTransport(@RequestBody @Validated Transport transport,
                                     HttpServletRequest request) {
        accessChecker.checkAccess(request);
        transportRepo.saveAndFlush(transport);
    }

    @DeleteMapping("/remove")
    public void removeTransport(@RequestBody @Validated Transport transport,
                                HttpServletRequest request) {
        accessChecker.checkAccess(request);
        transportRepo.delete(transport);
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TransportDto> getAllVehicles() {
        List<Transport> transport = transportRepo.findAll();
        List<TransportDto> transportDtos = new ArrayList<>();

        for (Transport t : transport) {
            transportDtos.add(transportMapper.modelToDto(t));
        }
        return transportDtos;
    }

    @GetMapping(value = "/find_by_type", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TransportDto> findTransportByType(@RequestParam("type") TransportTypeEnum type) {
        List<Transport> transport = transportRepo.findTransportByType(type);
        List<TransportDto> transportDtos = new ArrayList<>();

        for (Transport t : transport) {
            transportDtos.add(transportMapper.modelToDto(t));
        }
        return transportDtos;
    }

    @GetMapping(value = "/find_by_status", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TransportDto> findTransportByStatus(@RequestParam("status") boolean isFree) {
        List<Transport> transport = transportRepo.findTransportByStatus(isFree);
        List<TransportDto> transportDtos = new ArrayList<>();

        for (Transport t : transport) {
            transportDtos.add(transportMapper.modelToDto(t));
        }
        return transportDtos;
    }

    @PostMapping(value = "/find_by_parking", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TransportDto> findTransportByParkingZone(@RequestBody @Validated ParkingZone parkingZone) {
        List<Transport> transport = transportRepo.findTransportByParkingZone(parkingZone);
        List<TransportDto> transportDtos = new ArrayList<>();

        for (Transport t : transport) {
            transportDtos.add(transportMapper.modelToDto(t));
        }
        return transportDtos;
    }

    @GetMapping(value = "/find", produces = MediaType.APPLICATION_JSON_VALUE)
    public TransportDto getClosestTransport(@RequestParam("latitude") double latitude,
                                            @RequestParam("longitude") double longitude,
                                            @RequestParam("type") TransportTypeEnum type) {
        Transport transport = transportFinderService.getClosestTransport(latitude, longitude, type);
        return transportMapper.modelToDto(transport);
    }
}

package org.example.SystemCityTransportSB.controller;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.example.SystemCityTransportSB.Model.Dto.UserDto;
import org.example.SystemCityTransportSB.Model.Entity.User;
import org.example.SystemCityTransportSB.Model.mapper.UserMapper;
import org.example.SystemCityTransportSB.Repositories.UserRepo;
import org.example.SystemCityTransportSB.Security.ErrorCodeEnum;
import org.example.SystemCityTransportSB.Services.BusinessRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping("/users")
public class UserController {

    private final AccessCheckerImpl accessChecker;
    private final UserRepo userRepo;
    private final UserMapper userMapper;

    @Autowired
    public UserController(AccessCheckerImpl accessChecker,
                          UserRepo userRepo,
                          UserMapper userMapper) {
        this.accessChecker = accessChecker;
        this.userRepo = userRepo;
        this.userMapper = userMapper;
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserDto> getAllUsers(HttpServletRequest request) {
        accessChecker.checkAccess(request);
        List<User> users = userRepo.findAll();
        List<UserDto> userDtos = new ArrayList<>();

        for (User u : users) {
            userDtos.add(userMapper.modelToDto(u));
        }
        return userDtos;
    }

    @GetMapping("/current")
    public UserDto getCurrentUserInfo(HttpServletRequest request) {
        String token = request.getHeader(AUTHORIZATION);
        String jwtSecret = "secret";
        Claims claims = Jwts.parser().setSigningKey(jwtSecret)
                .parseClaimsJws(token).getBody();
        return userMapper.modelToDto(userRepo.findById(UUID.fromString(claims.getSubject()))
                .orElseThrow(() -> new BusinessRuntimeException(ErrorCodeEnum.USER_NOT_FOUND, claims.getSubject())));
    }

    @GetMapping("/find")
    public String findUsers(@RequestParam("email") @NotBlank String email,
                            HttpServletRequest request) {
        User user;
        UserDto userDto;

        accessChecker.checkAccess(request);
        user = userRepo.findByEmail(email);
        userDto = userMapper.modelToDto(user);
        return userDto.getId().toString();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDto getUserById(@PathVariable("id") @NotBlank String id,
                               HttpServletRequest request) {
        User user;
        UserDto userDto;
        UUID uuid;

        accessChecker.checkAccess(request);
        uuid = UUID.fromString(id);
        user = userRepo.findById(uuid).orElseThrow(() -> new BusinessRuntimeException(ErrorCodeEnum.USER_NOT_FOUND, id));
        userDto = userMapper.modelToDto(user);
        return userDto;
    }

    @DeleteMapping("/remove")
    public void deleteUser(@RequestBody @Validated User user,
                           HttpServletRequest request) {
        accessChecker.checkAccess(request);
        userRepo.delete(user);
    }

    @PutMapping("/update")
    public void addOrUpdateUser(@RequestBody @Validated User user,
                                HttpServletRequest request) {
        accessChecker.checkAccess(request);
        userRepo.saveAndFlush(user);
    }
}

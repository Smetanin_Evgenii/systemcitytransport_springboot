package org.example.SystemCityTransportSB.controller;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

@Service
@Transactional(readOnly = true)
public interface AccessChecker {

    void checkAccess(HttpServletRequest request);
}

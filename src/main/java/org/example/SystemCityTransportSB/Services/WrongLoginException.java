package org.example.SystemCityTransportSB.Services;

import org.springframework.stereotype.Service;

@Service
public class WrongLoginException extends Throwable {
    public WrongLoginException() {
    }
    public WrongLoginException(String s) {
        super(s);
    }
}

package org.example.SystemCityTransportSB.Services;

import org.springframework.stereotype.Service;

@Service
public class WrongPasswordException extends Throwable {
    public WrongPasswordException() {
    }
    public WrongPasswordException(String s) {
        super(s);
    }
}
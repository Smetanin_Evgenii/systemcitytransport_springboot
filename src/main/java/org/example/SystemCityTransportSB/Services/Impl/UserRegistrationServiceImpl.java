package org.example.SystemCityTransportSB.Services.Impl;

import lombok.NoArgsConstructor;
import org.example.SystemCityTransportSB.Model.Entity.User;
import org.example.SystemCityTransportSB.Repositories.UserRepo;
import org.example.SystemCityTransportSB.Services.Interface.UserRegistrationService;
import org.example.SystemCityTransportSB.Services.WrongLoginException;
import org.example.SystemCityTransportSB.Services.WrongPasswordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.example.SystemCityTransportSB.Model.Entity.UserRoleEnum.RENTER;

@NoArgsConstructor
@Service
@Transactional(readOnly = true)
public class UserRegistrationServiceImpl implements UserRegistrationService {

    UserRepo userRepo;
    BigDecimal bonusDeposit = new BigDecimal("1000");

    @Autowired
    public UserRegistrationServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    private static final Pattern PASSWORD_PATTERN = Pattern.compile("[A-Za-z0-9_]{0,19}");
    private static final Pattern LOGIN_PATTERN = Pattern.compile("[@A-Za-z0-9._%+-]{0,19}", Pattern.CASE_INSENSITIVE);

    @Override
    @Transactional
    public boolean registerNewUser(
            String email,
            String password,
            String confirmPassword) {

        boolean isSuccessful = false;
        UUID newUuid = UUID.randomUUID();
        User user;

        if (checkLoginPattern(email) && isLoginUnique(email) && checkPassword(password, confirmPassword)) {
            user = new User(newUuid, email, password, bonusDeposit, 0, RENTER);
            userRepo.saveAndFlush(user);
            isSuccessful = checkRegistrationSuccess(newUuid, email, password);
        }
        return isSuccessful;
    }

    @Override
    public boolean checkRegistrationSuccess(
            UUID id,
            String email,
            String password) {

        User user;
        boolean result;
        boolean isEmailCorrect;
        boolean isPasswordCorrect;

        user = userRepo.getById(id);
        isEmailCorrect = user.getEmail().equals(email);
        isPasswordCorrect = user.getPassword().equals(password);
        result = isEmailCorrect && isPasswordCorrect;
        return result;
    }

    @Override
    public boolean checkRegistrationSuccess(
            UUID id,
            String email) {

        User user;
        boolean isEmailCorrect;

        user = userRepo.getById(id);
        isEmailCorrect = user.getEmail().equals(email);
        return isEmailCorrect;
    }

    @Override
    public boolean checkLoginPattern(String email) {
        Matcher checkLogin = LOGIN_PATTERN.matcher(email);
        boolean isEmailCorrect = false;

        try {
            if (checkLogin.matches()) {
                isEmailCorrect = true;
            } else {
                throw new WrongLoginException("Incorrect email format");
            }
        } catch (WrongLoginException e) {
            System.out.println(e.getMessage());
        }
        return isEmailCorrect;
    }

    @Override
    public boolean isLoginUnique(String email) {
        boolean isLoginUnique;
        isLoginUnique = !userRepo.existsByEmail(email);
        return isLoginUnique;
    }

    @Override
    public boolean checkPassword(String password, String confirmPassword) {
        Matcher checkPassword = PASSWORD_PATTERN.matcher(password);
        boolean isPasswordCorrect = false;

        try {
            if (password.equals(confirmPassword)) {
                if (checkPassword.matches()) {
                    isPasswordCorrect = true;
                } else {
                    throw new WrongPasswordException("Incorrect password format");
                }
            } else {
                throw new WrongPasswordException("Passwords do not match");
            }
        } catch (WrongPasswordException e) {
            System.out.println(e.getMessage());
        }
        return isPasswordCorrect;
    }
}

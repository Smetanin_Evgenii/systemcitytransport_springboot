package org.example.SystemCityTransportSB.Services.Impl;

import lombok.NoArgsConstructor;
import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.example.SystemCityTransportSB.Model.Entity.TransportConditionEnum;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.example.SystemCityTransportSB.Repositories.ParkingZoneRepo;
import org.example.SystemCityTransportSB.Repositories.TransportRepo;
import org.example.SystemCityTransportSB.Services.Interface.TransportFinderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Service
@Transactional(readOnly = true)
public class TransportFinderServiceImpl implements TransportFinderService {

    ParkingZoneRepo parkingZoneRepo;
    TransportRepo transportRepo;
    Integer minChargePercent = 10;

    @Autowired
    public TransportFinderServiceImpl(ParkingZoneRepo parkingZoneRepo, TransportRepo transportRepo) {
        this.parkingZoneRepo = parkingZoneRepo;
        this.transportRepo = transportRepo;
    }

    public List<Object> sortParkingZones(double latitude, double longitude) {
        List<Object> output = new ArrayList<>();
        DistanceCalculatorServiceImpl calculator;
        List<ParkingZone> parkingZones;
        int elements;
        int[] indexes;
        double[] distancesFromUser;

        parkingZones = parkingZoneRepo.findAll();
        elements = parkingZones.size();
        distancesFromUser = new double[elements];
        calculator = new DistanceCalculatorServiceImpl();

        for (int i = 0; i < elements; i++) {
            distancesFromUser[i] = calculator.calculateDistanceToParkingZone(parkingZones.get(i), latitude, longitude);
        }

        indexes = new int[elements];
        for (int i = 0; i < elements; i++) {
            indexes[i] = i;
        }

        calculator.sort(indexes, distancesFromUser);

        output.add(parkingZones);
        output.add(indexes);
        return output;
    }

    @SuppressWarnings("unchecked")
    public Transport getClosestTransport(double latitude, double longitude, TransportTypeEnum type) {
        List<Object> data;
        List<ParkingZone> parkingZones;
        Transport transport;
        int[] indexes;

        data = sortParkingZones(latitude, longitude);
        parkingZones = (List<ParkingZone>) data.get(0);
        indexes = (int[]) data.get(1);

        transport = null;
        for (int index : indexes) {
            try {
                transport = transportRepo.findTransport(type, TransportConditionEnum.BAD, parkingZones.get(index), true, minChargePercent).get(0);
            } catch (IndexOutOfBoundsException ignored) {
            }
            if (transport != null) {
                break;
            }
        }
        return transport;
    }

    @SuppressWarnings("unchecked")
    public List<Transport> getClosestTransportList(double latitude, double longitude, TransportTypeEnum type) {
        List<Object> data;
        List<ParkingZone> parkingZones;
        List<Transport> vehicles = new ArrayList<>();
        List<Transport> temp;
        int[] indexes;

        data = sortParkingZones(latitude, longitude);
        parkingZones = (List<ParkingZone>) data.get(0);
        indexes = (int[]) data.get(1);

        a:
        for (int index : indexes) {
            try {
                temp = transportRepo.findTransport(type, TransportConditionEnum.BAD, parkingZones.get(index), true, minChargePercent);
                for (Transport t : temp) {
                    if (vehicles.size() < 10) {
                        vehicles.add(t);
                    } else {
                        break a;
                    }
                }
            } catch (IndexOutOfBoundsException ignored) {
            }
        }
        return vehicles;
    }

    @SuppressWarnings("unchecked")
    public List<Transport> getClosestTransportList(double latitude, double longitude) {
        List<Object> data;
        List<ParkingZone> parkingZones;
        List<Transport> vehicles = new ArrayList<>();
        List<Transport> temp;
        int[] indexes;

        data = sortParkingZones(latitude, longitude);
        parkingZones = (List<ParkingZone>) data.get(0);
        indexes = (int[]) data.get(1);

        a:
        for (int index : indexes) {
            try {
                temp = transportRepo.findAnyTransport(TransportConditionEnum.BAD, parkingZones.get(index), true, minChargePercent);
                for (Transport t : temp) {
                    if (vehicles.size() < 10) {
                        vehicles.add(t);
                    } else {
                        break a;
                    }
                }
            } catch (IndexOutOfBoundsException ignored) {
            }
        }
        return vehicles;
    }
}

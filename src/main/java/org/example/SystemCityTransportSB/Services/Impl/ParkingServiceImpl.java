package org.example.SystemCityTransportSB.Services.Impl;

import lombok.NoArgsConstructor;
import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.example.SystemCityTransportSB.Repositories.ParkingZoneRepo;
import org.example.SystemCityTransportSB.Services.Interface.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

import static org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum.BICYCLE;
import static org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum.MOTORIZEDSCOOTER;

@NoArgsConstructor
@Service
@Transactional(readOnly = true)
public class ParkingServiceImpl implements ParkingService {

    ParkingZoneRepo parkingZoneRepo;

    @Autowired
    public ParkingServiceImpl(ParkingZoneRepo parkingZoneRepo) {
        this.parkingZoneRepo = parkingZoneRepo;
    }

    @Override
    public ParkingZone getParkingZoneById(UUID id) {
        return parkingZoneRepo.getById(id);
    }

    @Override
    public ParkingZone getClosestParkingZone(double latitude,
                                             double longitude,
                                             TransportTypeEnum type) {
        DistanceCalculatorServiceImpl calculator;
        int elements;
        int[] indexes;
        double[] distancesFromUser;
        List<ParkingZone> parkingZones;
        ParkingZone parkingZone;

        calculator = new DistanceCalculatorServiceImpl();

        if (type.equals(BICYCLE)) {
            parkingZones = parkingZoneRepo.findAllByParkingBicyclesAllowedTrue();
        } else if (type.equals(MOTORIZEDSCOOTER)) {
            parkingZones = parkingZoneRepo.findAllByParkingMotorizedScootersAllowedTrue();
        } else {
            throw new RuntimeException("Incorrect transport type specified");
        }

        elements = parkingZones.size();
        distancesFromUser = new double[elements];
        indexes = new int[elements];

        for (int i = 0; i < elements; i++) {
            indexes[i] = i;
            distancesFromUser[i] = calculator.calculateDistanceToParkingZone(
                    parkingZones.get(i),
                    latitude,
                    longitude);
        }

        calculator.sort(indexes, distancesFromUser);
        parkingZone = (parkingZones.get(indexes[0]));
        return parkingZone;
    }

    @Override
    @Transactional
    public void addNewParkingZoneToDB(ParkingZone parkingZone) {
        parkingZoneRepo.saveAndFlush(parkingZone);
    }

    @Override
    @Transactional
    public void addNewParkingZoneToDB(double latitude,
                                      double longitude,
                                      int radiusMetres,
                                      boolean parkingBicyclesAllowed,
                                      boolean parkingMotorizedScootersAllowed) {
        ParkingZone newParkingZone;
        newParkingZone = new ParkingZone(
                UUID.randomUUID(),
                parkingBicyclesAllowed,
                parkingMotorizedScootersAllowed,
                radiusMetres,
                latitude,
                longitude
        );
        parkingZoneRepo.saveAndFlush(newParkingZone);
    }

    @Override
    @Transactional
    public void updateParkingZoneInfo(ParkingZone parkingZone) {
        parkingZoneRepo.saveAndFlush(parkingZone);
    }

    @Override
    @Transactional
    public void deleteParkingZone(String id) {
        UUID uuid;
        uuid = UUID.fromString(id);
        parkingZoneRepo.deleteById(uuid);
    }

    @Override
    @Transactional
    public void deleteParkingZone(ParkingZone parkingZone) {
        parkingZoneRepo.delete(parkingZone);
    }
}

package org.example.SystemCityTransportSB.Services.Impl;

import lombok.NoArgsConstructor;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.example.SystemCityTransportSB.Model.Entity.TransportConditionEnum;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.example.SystemCityTransportSB.Repositories.ParkingZoneRepo;
import org.example.SystemCityTransportSB.Repositories.TransportRepo;
import org.example.SystemCityTransportSB.Services.Interface.TransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@Service
@Transactional(readOnly = true)
public class TransportServiceImpl implements TransportService {

    TransportRepo transportRepo;
    ParkingZoneRepo parkingZoneRepo;

    @Autowired
    public TransportServiceImpl(
                                TransportRepo transportRepo,
                                ParkingZoneRepo parkingZoneRepo) {
        this.transportRepo = transportRepo;
        this.parkingZoneRepo = parkingZoneRepo;
    }

    @Override
    @Transactional
    public void updateDataBaseInfo(Transport transport) {
        transportRepo.saveAndFlush(transport);
    }

    @Override
    @Transactional
    public void addNewTransportToDB(
            TransportTypeEnum type,
            TransportConditionEnum condition,
            UUID parkingZoneId,
            Integer chargePercent,
            Integer maxSpeedKMPH) {

        Transport transport;
        transport = new Transport(
                getUniqueTransportId(type),
                type,
                condition,
                parkingZoneRepo.getById(parkingZoneId),
                true,
                chargePercent,
                maxSpeedKMPH
        );
        transportRepo.saveAndFlush(transport);
    }


    @Override
    public String getUniqueTransportId(TransportTypeEnum type) {

        int maxNum = 0;
        String newId;
        String vehicleType = "";
        int dashIndex = 0;

        List<Transport> transport = transportRepo.findTransportByType(type);
        for (Transport t : transport        ) {
            String id = t.getId();
            if (dashIndex == 0) {
                dashIndex = id.indexOf('-');
                vehicleType = id.substring(0, dashIndex + 1);
            }
            int number = Integer.parseInt(id.substring(dashIndex + 1));
            if (number > maxNum) {
                maxNum = number;
            }
        }
        maxNum++;
        newId = vehicleType + maxNum;
        return newId;
    }

    @Override
    @Transactional
    public void bookTransport(Transport transport) {
        transportRepo.bookTransport(transport.getId());
    }

    @Override
    @Transactional
    public void unBookTransport(Transport transport) {
        transportRepo.unBookTransport(transport.getId());
    }
}

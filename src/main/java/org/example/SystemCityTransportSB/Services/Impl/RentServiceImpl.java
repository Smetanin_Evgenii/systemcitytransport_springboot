package org.example.SystemCityTransportSB.Services.Impl;

import lombok.NoArgsConstructor;
import org.example.SystemCityTransportSB.Model.Dto.UserDto;
import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.example.SystemCityTransportSB.Model.Entity.PricePerMinute;
import org.example.SystemCityTransportSB.Model.Entity.Rent;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.example.SystemCityTransportSB.Repositories.ParkingZoneRepo;
import org.example.SystemCityTransportSB.Repositories.RentRepo;
import org.example.SystemCityTransportSB.Repositories.TransportRepo;
import org.example.SystemCityTransportSB.Security.ErrorCodeEnum;
import org.example.SystemCityTransportSB.Services.BusinessRuntimeException;
import org.example.SystemCityTransportSB.Services.Interface.RentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@Service
@Transactional(readOnly = true)
public class RentServiceImpl implements RentService {

    UserServiceImpl userService;
    TransportServiceImpl transportService;
    ParkingServiceImpl parkingService;
    String sdfPattern = "yyyy-MM-dd HH:mm:ss.SS";
    SimpleDateFormat sdf = new SimpleDateFormat(sdfPattern);
    Date now;
    String strDate;

    TransportRepo transportRepo;
    ParkingZoneRepo parkingZoneRepo;
    RentRepo rentRepo;
    TariffServiceImpl tariffService;

    @Autowired
    public RentServiceImpl(
            UserServiceImpl userService,
            TransportServiceImpl transportService,
            ParkingServiceImpl parkingService,
            TransportRepo transportRepo,
            ParkingZoneRepo parkingZoneRepo,
            RentRepo rentRepo,
            TariffServiceImpl tariffService) {

        this.userService = userService;
        this.transportService = transportService;
        this.parkingService = parkingService;
        this.transportRepo = transportRepo;
        this.parkingZoneRepo = parkingZoneRepo;
        this.rentRepo = rentRepo;
        this.tariffService = tariffService;
    }

    @Override
    @Transactional
    public synchronized Rent startRent(UserDto renter, Transport transport) {

        if (!transport.isFree()) {
            throw new BusinessRuntimeException(ErrorCodeEnum.TRANSPORT_OCCUPIED);
        }

        if (renter.getBalance().compareTo(BigDecimal.ZERO) < 0) {
            throw new BusinessRuntimeException(ErrorCodeEnum.INSUFFICIENT_FUNDS);
        }

        List<Rent> usersOpenRents = rentRepo.findByUserAndClosedIsFalse(renter);
        if (usersOpenRents.size() >= 3) {
            throw new BusinessRuntimeException(ErrorCodeEnum.MAX_NUMBER_OF_RENTS_EXCEEDED);
        }

        Rent rent = new Rent(
                UUID.randomUUID(),
                renter,
                transport,
                transport.getCurrentParkingZone()
        );

        transportService.bookTransport(transport);
        rentRepo.saveAndFlush(rent);
        return rent;
    }

    @Override
    @Transactional
    public void endRent(Rent rent, double latitude, double longitude) {

        UserDto renter;
        double[] coordinates;
        Transport transport;
        PricePerMinute pricePerMinute;
        ParkingZone endParkingZone;
        BigDecimal bill;
        boolean isRenterInParkingZone;
        boolean isParkingZoneSuitable;

        if (rent.isClosed()) {
            throw new BusinessRuntimeException(ErrorCodeEnum.INTERNAL_SERVER_ERROR);
        }

        renter = rent.getRenter();
        coordinates = new double[]{latitude, longitude};
        transport = rent.getTransport();

        endParkingZone = parkingService.getClosestParkingZone(
                coordinates[0],
                coordinates[1],
                transport.getType()
        );

        isRenterInParkingZone = endParkingZone.isRenterInParkingZone(coordinates[0], coordinates[1]);
        isParkingZoneSuitable = rent.isParkingZoneSuitable(transport, endParkingZone);
        pricePerMinute = tariffService.getPricePerMinute(transport.getType());

        if (isRenterInParkingZone && isParkingZoneSuitable) {

            rent.endRent(endParkingZone, pricePerMinute);
            transport.setCurrentParkingZone(endParkingZone);
            bill = rent.getBill();

            now = new Date();
            strDate = sdf.format(now);

            userService.userDtoPayBill(renter, bill);
            userService.userDtoIncrementRides(renter);
            userService.updateDataBaseInfo(renter);

            rentRepo.saveAndFlush(rent);
            transportRepo.changeParkingZone(transport.getId(), endParkingZone);
            transportService.unBookTransport(transport);

        } else if (!isRenterInParkingZone) {
            throw new BusinessRuntimeException(ErrorCodeEnum.NOT_IN_PARKING_ZONE);
        } else {
            System.out.println("Parking a " + transport.getType().toString().toLowerCase() + " is not allowed here!");
        }
    }
}

package org.example.SystemCityTransportSB.Services.Impl;

import lombok.NoArgsConstructor;
import org.example.SystemCityTransportSB.Model.Entity.PricePerMinute;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.example.SystemCityTransportSB.Repositories.PricePerMinuteRepo;
import org.example.SystemCityTransportSB.Services.Interface.TariffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@NoArgsConstructor
@Service
@Transactional(readOnly = true)
public class TariffServiceImpl implements TariffService {
    PricePerMinuteRepo pricePerMinuteRepo;

    @Autowired
    public TariffServiceImpl(PricePerMinuteRepo pricePerMinuteRepo) {
        this.pricePerMinuteRepo = pricePerMinuteRepo;
    }

    @Override
    public PricePerMinute getPricePerMinute(TransportTypeEnum type) {
        return pricePerMinuteRepo.findByType(type);
    }
}

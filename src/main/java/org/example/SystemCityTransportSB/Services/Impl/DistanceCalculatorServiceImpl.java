package org.example.SystemCityTransportSB.Services.Impl;

import lombok.NoArgsConstructor;
import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.example.SystemCityTransportSB.Repositories.ParkingZoneRepo;
import org.example.SystemCityTransportSB.Services.Interface.DistanceCalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@NoArgsConstructor
@Service
@Transactional(readOnly = true)
public class DistanceCalculatorServiceImpl implements DistanceCalculatorService {

    ParkingZoneRepo parkingZoneRepo;

    @Autowired
    public DistanceCalculatorServiceImpl(ParkingZoneRepo parkingZoneRepo) {
        this.parkingZoneRepo = parkingZoneRepo;
    }

    @Override
    public void sort(int[] indexes, double[] distancesFromUserUnsorted) {
        int n = indexes.length;
        double distancesTemp = 0;
        int indexesTemp = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (distancesFromUserUnsorted[j - 1] > distancesFromUserUnsorted[j]) {

                    distancesTemp = distancesFromUserUnsorted[j - 1];
                    indexesTemp = indexes[j - 1];

                    distancesFromUserUnsorted[j - 1] = distancesFromUserUnsorted[j];
                    indexes[j - 1] = indexes[j];

                    distancesFromUserUnsorted[j] = distancesTemp;
                    indexes[j] = indexesTemp;
                }
            }
        }
    }

    @Override
    public double calculateDistanceToParkingZone(ParkingZone parkingZone, double userLatitude, double userLongitude) {
        double parkingZoneLatitude;
        double parkingZoneLongitude;
        double distanceInMetres;
        double latitudinalDistance;
        double longitudinalDistance;

        parkingZoneLatitude = parkingZone.getLatitude();
        parkingZoneLongitude = parkingZone.getLongitude();
        latitudinalDistance = Math.pow((Math.abs(parkingZoneLatitude - userLatitude) * 111000), 2);
        longitudinalDistance = Math.pow(Math.abs(111000 * Math.cos(parkingZoneLongitude * (Math.PI / 180)) * (parkingZoneLongitude - userLongitude)), 2);

        distanceInMetres = Math.sqrt(latitudinalDistance + longitudinalDistance);
        return distanceInMetres;
    }
}

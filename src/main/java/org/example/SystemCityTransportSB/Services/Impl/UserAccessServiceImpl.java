package org.example.SystemCityTransportSB.Services.Impl;

import lombok.NoArgsConstructor;
import org.example.SystemCityTransportSB.Model.Entity.User;
import org.example.SystemCityTransportSB.Repositories.UserRepo;
import org.example.SystemCityTransportSB.Services.Interface.UserAccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@NoArgsConstructor
@Service
@Transactional(readOnly = true)
public class UserAccessServiceImpl implements UserAccessService {

    UserRepo userRepo;

    @Autowired
    public UserAccessServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public User login(String login, String password) {
        User user = null;

        if (userRepo.findByEmail(login).getPassword().equals(password)) {
            user = userRepo.findByEmail(login);
        }
        return user;
    }

    @Override
    public User createUserFromId(String id) {
        UUID uuid;
        User user;

        uuid = UUID.fromString(id);
        user = userRepo.getById(uuid);
        return user;
    }
}

package org.example.SystemCityTransportSB.Services.Impl;

import lombok.NoArgsConstructor;
import org.example.SystemCityTransportSB.Model.Dto.UserDto;
import org.example.SystemCityTransportSB.Model.Entity.User;
import org.example.SystemCityTransportSB.Repositories.UserRepo;
import org.example.SystemCityTransportSB.Services.Interface.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.UUID;

@NoArgsConstructor
@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

    UserRepo userRepo;

    @Autowired
    public UserServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    @Transactional
    public void updateDataBaseInfo(UserDto userDto) {
        UUID id;
        User user;

        id = userDto.getId();
        user = userRepo.getById(id);

        user.setBalance(userDto.getBalance());
        user.setNumberOfRides(userDto.getNumberOfRides());
        userRepo.saveAndFlush(user);
    }

    @Override
    @Transactional
    public void updateDataBaseInfo(User user) {
        userRepo.saveAndFlush(user);
    }

    @Override
    public void userDtoPayBill(UserDto renter, BigDecimal bill) {

        renter.setBalance(renter.getBalance().subtract(bill));
        if (renter.getBalance().compareTo(BigDecimal.ZERO) < 0) {
            System.out.println("your balance is negative, please make a deposit in the amount of "
                    + Math.abs(Double.parseDouble(renter.getBalance().toString())));
        }
    }

    @Override
    public void userDtoMakeDeposit(UserDto renter, BigDecimal deposit) {
        BigDecimal balance = renter.getBalance();
        BigDecimal newBalance = balance.add(deposit);
        renter.setBalance(newBalance);
        updateDataBaseInfo(renter);
    }

    @Override
    public void userDtoIncrementRides(UserDto renter) {
        renter.setNumberOfRides(renter.getNumberOfRides() + 1);
        updateDataBaseInfo(renter);
    }
}

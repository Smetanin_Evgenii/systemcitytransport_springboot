package org.example.SystemCityTransportSB.Services.Interface;

import org.example.SystemCityTransportSB.Model.Entity.PricePerMinute;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public interface TariffService {

    PricePerMinute getPricePerMinute(TransportTypeEnum type);

}

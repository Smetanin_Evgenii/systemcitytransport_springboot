package org.example.SystemCityTransportSB.Services.Interface;

import org.example.SystemCityTransportSB.Model.Dto.UserDto;
import org.example.SystemCityTransportSB.Model.Entity.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
@Transactional(readOnly = true)
public interface UserService {

    @Transactional
    void updateDataBaseInfo(UserDto userDto);

    @Transactional
    void updateDataBaseInfo(User user);

    void userDtoPayBill(UserDto renter, BigDecimal bill);

    void userDtoMakeDeposit(UserDto renter, BigDecimal deposit);

    void userDtoIncrementRides(UserDto renter);
}

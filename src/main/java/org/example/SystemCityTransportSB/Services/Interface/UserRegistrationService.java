package org.example.SystemCityTransportSB.Services.Interface;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional(readOnly = true)
public interface UserRegistrationService {

    @Transactional
    boolean registerNewUser(String email, String password, String confirmPassword);

    boolean checkRegistrationSuccess(
            UUID id,
            String email,
            String password);

    boolean checkRegistrationSuccess(
            UUID id,
            String email);

    boolean checkLoginPattern(String email);

    boolean isLoginUnique(String email);

    boolean checkPassword(String password, String confirmPassword);
}

package org.example.SystemCityTransportSB.Services.Interface;

import org.example.SystemCityTransportSB.Model.Entity.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public interface UserAccessService {

    User login(String login, String password);

    User createUserFromId(String id);
}

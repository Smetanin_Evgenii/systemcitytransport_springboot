package org.example.SystemCityTransportSB.Services.Interface;

import org.example.SystemCityTransportSB.Model.Dto.UserDto;
import org.example.SystemCityTransportSB.Model.Entity.Rent;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public interface RentService {

    @Transactional
    Rent startRent(UserDto renter, Transport transport);

    @Transactional
    void endRent(Rent currentRent, double latitude, double longitude);

}

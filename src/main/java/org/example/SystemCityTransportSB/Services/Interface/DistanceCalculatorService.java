package org.example.SystemCityTransportSB.Services.Interface;

import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public interface DistanceCalculatorService {

    void sort(int[] indexes, double[] distancesFromUserUnsorted);

    double calculateDistanceToParkingZone(
            ParkingZone parkingZone,
            double userLatitude,
            double userLongitude);
}

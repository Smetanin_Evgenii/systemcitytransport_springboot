package org.example.SystemCityTransportSB.Services.Interface;

import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.example.SystemCityTransportSB.Model.Entity.TransportConditionEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional(readOnly = true)
public interface TransportService {

    @Transactional
    void updateDataBaseInfo(Transport transport);

    @Transactional
    void addNewTransportToDB(TransportTypeEnum type, TransportConditionEnum condition, UUID parkingZoneId, Integer chargePercent, Integer maxSpeedKMPH);

    String getUniqueTransportId(TransportTypeEnum type);

    @Transactional
    void bookTransport(Transport transport);

    @Transactional
    void unBookTransport(Transport transport);
}

package org.example.SystemCityTransportSB.Services.Interface;

import org.example.SystemCityTransportSB.Model.Entity.ParkingZone;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional(readOnly = true)
public interface ParkingService {

    ParkingZone getParkingZoneById(UUID id);

    ParkingZone getClosestParkingZone(double latitude, double longitude, TransportTypeEnum type);

    @Transactional
    void addNewParkingZoneToDB(ParkingZone parkingZone);

    @Transactional
    void addNewParkingZoneToDB(double latitude,
                               double longitude,
                               int radiusMetres,
                               boolean parkingBicyclesAllowed,
                               boolean parkingMotorizedScootersAllowed);

    @Transactional
    void updateParkingZoneInfo(ParkingZone parkingZone);

    @Transactional
    void deleteParkingZone(String id);

    @Transactional
    void deleteParkingZone(ParkingZone parkingZone);



}

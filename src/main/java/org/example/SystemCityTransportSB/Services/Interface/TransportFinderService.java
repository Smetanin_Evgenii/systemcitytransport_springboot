package org.example.SystemCityTransportSB.Services.Interface;

import org.example.SystemCityTransportSB.Model.Entity.Transport;
import org.example.SystemCityTransportSB.Model.Entity.TransportTypeEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public interface TransportFinderService {

    List<Object> sortParkingZones(double latitude, double longitude
//                                  TransportTypeEnum type
    );

    Transport getClosestTransport(double latitude, double longitude, TransportTypeEnum type);

    List<Transport> getClosestTransportList(double latitude, double longitude, TransportTypeEnum type);

    List<Transport> getClosestTransportList(double latitude, double longitude);
}

package org.example.SystemCityTransportSB.Services;

import lombok.Getter;
import org.example.SystemCityTransportSB.Security.ErrorCodeEnum;

@Getter
public class BusinessRuntimeException extends RuntimeException {
    private ErrorCodeEnum errorCode;

    public BusinessRuntimeException(ErrorCodeEnum errorCode,
                                    Throwable cause, Object... args) {
        super(errorCode.getMessage(args), cause);
        this.errorCode = errorCode;
    }

    public BusinessRuntimeException(ErrorCodeEnum errorCode, Object... args) {
        super(errorCode.getMessage(args));
        this.errorCode = errorCode;
    }
}
